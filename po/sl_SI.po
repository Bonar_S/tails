# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Dušan <dusan.k@zoho.com>, 2014
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-01-09 16:26+0100\n"
"PO-Revision-Date: 2015-02-12 14:52+0000\n"
"Last-Translator: runasand <runa.sandvik@gmail.com>\n"
"Language-Team: Slovenian (Slovenia) (http://www.transifex.com/projects/p/"
"torproject/language/sl_SI/)\n"
"Language: sl_SI\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n"
"%100==4 ? 2 : 3);\n"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:43
msgid "Tor is ready"
msgstr "Tor je pripravljen"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:44
msgid "You can now access the Internet."
msgstr "Sedaj lahko dostopate do omrežja"

#: config/chroot_local-includes/etc/whisperback/config.py:64
#, fuzzy, python-format
msgid ""
"<h1>Help us fix your bug!</h1>\n"
"<p>Read <a href=\"%s\">our bug reporting instructions</a>.</p>\n"
"<p><strong>Do not include more personal information than\n"
"needed!</strong></p>\n"
"<h2>About giving us an email address</h2>\n"
"<p>\n"
"Giving us an email address allows us to contact you to clarify the problem. "
"This\n"
"is needed for the vast majority of the reports we receive as most reports\n"
"without any contact information are useless. On the other hand it also "
"provides\n"
"an opportunity for eavesdroppers, like your email or Internet provider, to\n"
"confirm that you are using Tails.\n"
"</p>\n"
msgstr ""
"<h1>Pomagajte nam popraviti vašega hrošča! </h1>\n"
"<p>Preberite<a href=\"%s\">naša navodila za poročanje o hrošču</a>.</p>\n"
"<p><strong>Ne vključujte več osebnih podatkov kot je\n"
"potrebno!</strong></p>\n"
"<h2>O dajanju naslova e-pošte</h2>\n"
"Če vam ni odveč odkriti nekaj delčkov svoje identitete\n"
"razvijalcem Sledi, nam lahko podate svoj e-naslov,\n"
"da vas vprašamo o podrobnostih hrošča. Dodan\n"
"javni PGP ključ nam pomaga dešifrirati podobna bodoča\n"
"sporočila.</p>\n"
"<p>Vsakdo, ki vidi ta odgovor, bo verjetno zaključil, da ste\n"
"uporabnik Sledi. Čas je, da se vprašamo koliko zaupamo našim\n"
"Internet in poštnim ponudnikom?</p>\n"

#: config/chroot_local-includes/usr/local/bin/electrum:14
msgid "Persistence is disabled for Electrum"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/electrum:16
msgid ""
"When you reboot Tails, all of Electrum's data will be lost, including your "
"Bitcoin wallet. It is strongly recommended to only run Electrum when its "
"persistence feature is activated."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/electrum:18
msgid "Do you want to start Electrum anyway?"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/electrum:20
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:36
msgid "_Launch"
msgstr "_Zagon"

#: config/chroot_local-includes/usr/local/bin/electrum:21
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:37
msgid "_Exit"
msgstr "_Izhod"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:136
msgid "OpenPGP encryption applet"
msgstr "Odpri PGP šifrirni programček"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:139
msgid "Exit"
msgstr "Izhod"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:141
msgid "About"
msgstr "Vizitka"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:192
msgid "Encrypt Clipboard with _Passphrase"
msgstr "Šifriranje odložišča z _Geslom za ključe"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:195
msgid "Sign/Encrypt Clipboard with Public _Keys"
msgstr "Vpis / Šifriranje odložišča z javnimi _Ključi"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:200
msgid "_Decrypt/Verify Clipboard"
msgstr "_Dešifriranje/Preverite Odložišče"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:204
msgid "_Manage Keys"
msgstr "_Upravljanje s Ključi"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:208
msgid "_Open Text Editor"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/gpgApplet:252
msgid "The clipboard does not contain valid input data."
msgstr "Odložišče ne vsebuje veljavnih vhodnih podatkov"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:303
#: config/chroot_local-includes/usr/local/bin/gpgApplet:305
#: config/chroot_local-includes/usr/local/bin/gpgApplet:307
msgid "Unknown Trust"
msgstr "Neznan Skrbnik"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:309
msgid "Marginal Trust"
msgstr "Mejni Skrbnik"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:311
msgid "Full Trust"
msgstr "Zaupanja vreden Skrbnik"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:313
msgid "Ultimate Trust"
msgstr "Dokončni Skrbnik"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:366
msgid "Name"
msgstr "Ime"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:367
msgid "Key ID"
msgstr "Ključ ID"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:368
msgid "Status"
msgstr "Stanje"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:400
msgid "Fingerprint:"
msgstr "Prstni odtis:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:403
msgid "User ID:"
msgid_plural "User IDs:"
msgstr[0] "Uporbnik ID:"
msgstr[1] "Uporabnika ID:"
msgstr[2] "Uporabniki ID:"
msgstr[3] "User IDs:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:433
msgid "None (Don't sign)"
msgstr "Nobeden (Brez podpisa)"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:496
msgid "Select recipients:"
msgstr "Izbira prejemnikov:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:504
msgid "Hide recipients"
msgstr "Skrij prejemnike"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:507
msgid ""
"Hide the user IDs of all recipients of an encrypted message. Otherwise "
"anyone that sees the encrypted message can see who the recipients are."
msgstr ""
"Skrij ID uporabnika vsem prejemnikom šifriranega sporočila. Drugače lahko "
"vsak, ki šifrirano sporočilo vidi, ve kdo je prejemnik."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:513
msgid "Sign message as:"
msgstr "Podpiši sporočilo kot:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:517
msgid "Choose keys"
msgstr "Izberite ključe"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:557
msgid "Do you trust these keys?"
msgstr "Ali zaupate tem ključem?"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:560
msgid "The following selected key is not fully trusted:"
msgid_plural "The following selected keys are not fully trusted:"
msgstr[0] "Sledeči izbran ključ ni vreden popolnega zaupanja:"
msgstr[1] "Sledeča izbrana ključa nista vredna popolnega zaupanja:"
msgstr[2] "Sledeči izbrani ključi niso vredni popolnega zaupanja:"
msgstr[3] "Sledeči izbrani ključi niso vredni popolnega zaupanja:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:578
msgid "Do you trust this key enough to use it anyway?"
msgid_plural "Do you trust these keys enough to use them anyway?"
msgstr[0] "Ali dovolj zaupate temu ključu, da ga vseeno uporabite?"
msgstr[1] "Ali dovolj zaupate tema ključema, da ju vseeno uporabite?"
msgstr[2] "Ali dovolj zaupate tem ključem, da jih vseeno uporabite?"
msgstr[3] "Ali dovolj zaupate tem ključem, da jih vseeno uporabite?"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:591
msgid "No keys selected"
msgstr "Ključ ni izbran"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:593
msgid ""
"You must select a private key to sign the message, or some public keys to "
"encrypt the message, or both."
msgstr ""
"Izbrati morate zasebni ključ za podpisovanje sporočila, ali kateri javni "
"ključi za šifriranje sporočila, ali pa oboje."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:621
msgid "No keys available"
msgstr "Ni uporabnega ključa"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:623
msgid ""
"You need a private key to sign messages or a public key to encrypt messages."
msgstr ""
"Rabite zasebni ključ za podpis sporočila ali javni ključ za šifriranje le "
"tega."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:751
msgid "GnuPG error"
msgstr "GnuPG napaka"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:772
msgid "Therefore the operation cannot be performed."
msgstr "Zato ni mogoče izvesti operacijo."

#: config/chroot_local-includes/usr/local/bin/gpgApplet:822
msgid "GnuPG results"
msgstr "GnuPG rezultati"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:828
msgid "Output of GnuPG:"
msgstr "Izhod iz GnuPG:"

#: config/chroot_local-includes/usr/local/bin/gpgApplet:853
msgid "Other messages provided by GnuPG:"
msgstr "Druga sporočila ponujena od GnuPG:"

#: config/chroot_local-includes/usr/local/lib/shutdown-helper-applet:39
msgid "Shutdown Immediately"
msgstr "Nemudoma Ugasni"

#: config/chroot_local-includes/usr/local/lib/shutdown-helper-applet:40
msgid "Reboot Immediately"
msgstr "Nemudoma znova zaženi"

#: config/chroot_local-includes/usr/local/bin/tails-about:16
msgid "not available"
msgstr "ni primeren"

#: config/chroot_local-includes/usr/local/bin/tails-about:19
#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:1
msgid "Tails"
msgstr "Sledi"

#: config/chroot_local-includes/usr/local/bin/tails-about:24
msgid "The Amnesic Incognito Live System"
msgstr "aktivni sistem The Amnesic Incognito"

#: config/chroot_local-includes/usr/local/bin/tails-about:25
#, python-format
msgid ""
"Build information:\n"
"%s"
msgstr ""
"Oblikuje informacije\n"
"%s"

#: config/chroot_local-includes/usr/local/bin/tails-about:27
#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:1
msgid "About Tails"
msgstr "Vizitka Sledi"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:118
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:124
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:128
msgid "Your additional software"
msgstr "Vaša dodatna programska oprema"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:119
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:129
msgid ""
"The upgrade failed. This might be due to a network problem. Please check "
"your network connection, try to restart Tails, or read the system log to "
"understand better the problem."
msgstr ""
"Nadgradnja ni uspela. To je lahko zaradi težav v omrežju. Preverite omrežno "
"povezavo, poskusite ponovno zagnati Sledi, ali pa preberite sistemski "
"dnevnik za boljše razumevanje problema."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:125
msgid "The upgrade was successful."
msgstr "Nadgradnja je bila uspešna."

#: config/chroot_local-includes/usr/local/bin/tails-htp-notify-user:52
msgid "Synchronizing the system's clock"
msgstr "Sinhronizacija sistemske ure"

#: config/chroot_local-includes/usr/local/bin/tails-htp-notify-user:53
msgid ""
"Tor needs an accurate clock to work properly, especially for Hidden "
"Services. Please wait..."
msgstr ""
"Tor potrebuje točno uro za pravilno delovanje, predvsem za Skrite storitve. "
"Prosimo, počakajte ..."

#: config/chroot_local-includes/usr/local/bin/tails-htp-notify-user:87
msgid "Failed to synchronize the clock!"
msgstr "Sinhronizacija ure ni bila uspešna!"

#: config/chroot_local-includes/usr/local/sbin/tails-restricted-network-detector:38
msgid "Network connection blocked?"
msgstr "Omrežna povezava blokirana?"

#: config/chroot_local-includes/usr/local/sbin/tails-restricted-network-detector:40
msgid ""
"It looks like you are blocked from the network. This may be related to the "
"MAC spoofing feature. For more information, see the <a href=\\\"file:///usr/"
"share/doc/tails/website/doc/first_steps/startup_options/mac_spoofing.en."
"html#blocked\\\">MAC spoofing documentation</a>."
msgstr ""
"Izgleda, da vas blokira omrežje. To se lahko nanaša na funkcijo MAC "
"sleparjenje. Za več informacij si oglejte < href=\\\"file:///usr/share/doc/"
"tails/website/doc/first_steps/startup_options/mac_spoofing.en.html#blocked\\"
"\">dokumentacija MAC sleparjenje </a>."

#: config/chroot_local-includes/usr/local/bin/tails-security-check:151
msgid "This version of Tails has known security issues:"
msgstr "Ta verzija Sledi ima znane varnostne izhode:"

#: config/chroot_local-includes/usr/local/sbin/tails-spoof-mac:50
#, sh-format
msgid "Network card ${nic} disabled"
msgstr "Omrežna kartra ${nic} onemogočena"

#: config/chroot_local-includes/usr/local/sbin/tails-spoof-mac:51
#, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}) so it is "
"temporarily disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing. See the <a "
"href='file:///usr/share/doc/tails/website/doc/first_steps/startup_options/"
"mac_spoofing.en.html'>documentation</a>."
msgstr ""
"Sleparjenje MAC mrežne kartice neuspešno ${nic_name} ($ {nic}), tako da je "
"začasno onemogočeno.\n"
"Morda vam je ljubše, da resetirate Sledi in onemogočite sleparjenje MAC. "
"Glejte < href='file:///usr/share/doc/tails/website/doc/first_steps/"
"startup_options/mac_spoofing.en.html'>dokumentacijo</a>."

#: config/chroot_local-includes/usr/local/sbin/tails-spoof-mac:60
msgid "All networking disabled"
msgstr "Vsa omrežja onemogočena"

#: config/chroot_local-includes/usr/local/sbin/tails-spoof-mac:61
#, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}). The error "
"recovery also failed so all networking is disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing. See the <a "
"href='file:///usr/share/doc/first_steps/startup_options/mac_spoofing.en."
"html'>documentation</a>."
msgstr ""
"Sleparjenje MAC omrežne kartice neuspešno ${nic_name} (${nic}). Okrevanje po "
"napaki tudi neuspešno, zato so vsa omrežja onemogočena\n"
"Morda vam je ljubše, da resetirate Sledi in onemogočite sleparjenje MAC. "
"Glejte <a href='file:///usr/share/doc/first_steps/startup_options/"
"mac_spoofing.en.html'>dokumentacijo</a>."

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:19
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:22
msgid "error:"
msgstr "napaka:"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:20
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:23
msgid "Error"
msgstr "Napaka"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:40
msgid ""
"<b>Not enough memory available to check for upgrades.</b>\n"
"\n"
"Make sure this system satisfies the requirements for running Tails.\n"
"See file:///usr/share/doc/tails/website/doc/about/requirements.en.html\n"
"\n"
"Try to restart Tails to check for upgrades again.\n"
"\n"
"Or do a manual upgrade.\n"
"See https://tails.boum.org/doc/first_steps/upgrade#manual"
msgstr ""
"<b>Ni dovolj spomina za preverjanje nadgradnje.</b>\n"
"\n"
"Prepričajte se, da  ta sistem ustreza zahtevam za zagon Sledi.\n"
"Glejte datoteko:///usr/share/doc/tails/website/doc/about/requirements.en."
"html\n"
"\n"
"Poskusite resetirati Sledi za ponovno preverjanje nadgradnje.\n"
"\n"
"Ali izvedite nadgradnjo ročno\n"
"Glejte https://tails.boum.org/doc/first_steps/upgrade#manual"

#: config/chroot_local-includes/usr/local/bin/tails-virt-notify-user:53
msgid "Warning: virtual machine detected!"
msgstr "Opozorilo: Zaznan virtualni stroj!"

#: config/chroot_local-includes/usr/local/bin/tails-virt-notify-user:55
msgid ""
"Both the host operating system and the virtualization software are able to "
"monitor what you are doing in Tails."
msgstr ""
"Operacijski sistem gostitelja in programska oprema za virtualizacijo lahko "
"spremljata, kaj počnete v Sledi."

#: config/chroot_local-includes/usr/local/bin/tails-virt-notify-user:57
#, fuzzy
msgid ""
"<a href='file:///usr/share/doc/tails/website/doc/advanced_topics/"
"virtualization.en.html#security'>Learn more...</a>"
msgstr ""
"<a href='file:///usr/share/doc/tails/website/doc/advanced_topics/"
"virtualization.en.html'>Več ...</a>"

#: config/chroot_local-includes/usr/local/bin/tor-browser:29
msgid "Tor is not ready"
msgstr "Tor ni pripravljen"

#: config/chroot_local-includes/usr/local/bin/tor-browser:30
msgid "Tor is not ready. Start Tor Browser anyway?"
msgstr "Tor ni pripravljen. Zaženem Tor brskalnik vseeno?"

#: config/chroot_local-includes/usr/local/bin/tor-browser:31
msgid "Start Tor Browser"
msgstr "Zagon Tor brskalnik"

#: config/chroot_local-includes/usr/local/bin/tor-browser:32
msgid "Cancel"
msgstr "Opusti"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:33
msgid "Do you really want to launch the Unsafe Browser?"
msgstr "Resnično želite zagnati nezanesljiv Brskalnik?"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:35
msgid ""
"Network activity within the Unsafe Browser is <b>not anonymous</b>. Only use "
"the Unsafe Browser if necessary, for example if you have to login or "
"register to activate your Internet connection."
msgstr ""
"Omrežne aktivnosti z nezanesljivim Brskalnikom <b>niso anonimne</b>. Le v "
"nujnih primerih uporabite nezanesljiv Brskalnik, npr.: če se morate "
"prijaviti ali registrirati za aktiviranje omrežne povezave."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:47
msgid "Starting the Unsafe Browser..."
msgstr "Zagon nezanesljivega Brskalnika ..."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:48
msgid "This may take a while, so please be patient."
msgstr "To lahko traja, zato bodite potrpežjivi."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:53
msgid "Shutting down the Unsafe Browser..."
msgstr "Ugašanje nezanesljivega Brskalnika .."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:54
msgid ""
"This may take a while, and you may not restart the Unsafe Browser until it "
"is properly shut down."
msgstr ""
"To lahko traja in ne smete ponoviti zagona nezanesljivega Brskalnika dokler "
"ni pravilno ugasnjen."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:66
msgid "Failed to restart Tor."
msgstr "Ponoven zagon Tor-a neuspešen. "

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:85
#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:1
msgid "Unsafe Browser"
msgstr "Nezanesljiv Brskalnik"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:93
msgid ""
"Another Unsafe Browser is currently running, or being cleaned up. Please "
"retry in a while."
msgstr ""
"Drugi nezanesljiv Brskalnik se trenutno izvaja ali se čisti. Poskusite malo "
"kasneje. "

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:105
msgid ""
"NetworkManager passed us garbage data when trying to deduce the clearnet DNS "
"server."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:115
msgid ""
"No DNS server was obtained through DHCP or manually configured in "
"NetworkManager."
msgstr ""
"Noben DNS server ni bil pridobljen iz DHCP ali ročno nastavljen v "
"NetworkManager."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:123
msgid "Failed to setup chroot."
msgstr "Neuspešna nastavitev chroot."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:129
#, fuzzy
msgid "Failed to configure browser."
msgstr "Ponoven zagon Tor-a neuspešen. "

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:134
#, fuzzy
msgid "Failed to run browser."
msgstr "Ponoven zagon Tor-a neuspešen. "

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:35
msgid "I2P failed to start"
msgstr "I2P se ni zagnal"

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:36
msgid ""
"Something went wrong when I2P was starting. Check the logs in /var/log/i2p "
"for more information."
msgstr ""
"Pri zagonu I2P je nekaj narobe. Preverite dnevnik log v /var/log/i2p za "
"nadaljne informacije"

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:53
msgid "I2P's router console is ready"
msgstr "I2P konzola usmerjevalnika je pripravljena"

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:54
#, fuzzy
msgid "You can now access I2P's router console in the I2P Browser."
msgstr "Konzolo I2P usmerjevalnika lahko dosegate na http://127.0.0.1:7657."

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:59
msgid "I2P is not ready"
msgstr "I2P ni propravljen"

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:60
#, fuzzy
msgid ""
"Eepsite tunnel not built within six minutes. Check the router console in the "
"I2P Browser or the logs in /var/log/i2p for more information. Reconnect to "
"the network to try again."
msgstr ""
"Eepsite predor ni zgrajena v šestih minutah. Preverite usmerjevalnik konzolo "
"na http://127.0.0.1:7657/logs ali dnevnike v / var / log / i2p za več "
"informacij. Ponovno se priklopite na omrežje in poskusite znova."

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:72
msgid "I2P is ready"
msgstr "I2P je pripravljen"

#: config/chroot_local-includes/usr/local/sbin/tails-i2p:73
msgid "You can now access services on I2P."
msgstr "sedaj lahko dostopate do storitev I2P"

#: ../config/chroot_local-includes/etc/skel/Desktop/Report_an_error.desktop.in.h:1
msgid "Report an error"
msgstr "Sporočite napako"

#: ../config/chroot_local-includes/etc/skel/Desktop/tails-documentation.desktop.in.h:1
#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:1
msgid "Tails documentation"
msgstr "Dokumentacija Sledi"

#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:2
msgid "Learn how to use Tails"
msgstr "Učenje uporabe Sledi"

#: ../config/chroot_local-includes/usr/share/applications/i2p-browser.desktop.in.h:1
msgid "Anonymous overlay network browser"
msgstr "Brskalnik anonimnega prikrivanja"

#: ../config/chroot_local-includes/usr/share/applications/i2p-browser.desktop.in.h:2
msgid "I2P Browser"
msgstr "I2P brskalnik"

#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:2
msgid "Learn more about Tails"
msgstr "Več o Sledi"

#: ../config/chroot_local-includes/usr/share/applications/tails-reboot.desktop.in.h:1
msgid "Reboot"
msgstr "Ponoven zagon"

#: ../config/chroot_local-includes/usr/share/applications/tails-reboot.desktop.in.h:2
msgid "Immediately reboot computer"
msgstr "Nemudoma ponovno zaženite računalnik"

#: ../config/chroot_local-includes/usr/share/applications/tails-shutdown.desktop.in.h:1
msgid "Power Off"
msgstr "Izklop"

#: ../config/chroot_local-includes/usr/share/applications/tails-shutdown.desktop.in.h:2
msgid "Immediately shut down computer"
msgstr "Nemudoma ugasnite računalnik"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:1
msgid "Tor Browser"
msgstr "Tor Iskalnik"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:2
msgid "Anonymous Web Browser"
msgstr "Anonimni web brskalnik"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:2
msgid "Browse the World Wide Web without anonymity"
msgstr "Brskajte po spletu brez anonimnosti"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:3
msgid "Unsafe Web Browser"
msgstr "Nezanesljiv web brskalnik"

#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:2
msgid "Tails specific tools"
msgstr "posebna orodja Sledi"
