# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-11-04 11:54+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Title ##
#, no-wrap
msgid "New features"
msgstr "Nouveautés"

#. type: Bullet: '- '
msgid ""
"You can now start Tails in [[offline mode|doc/first_steps/startup_options/"
"offline_mode]] to disable all networking for additional security. Doing so "
"can be useful when working on sensitive documents."
msgstr ""
"Vous pouvez maintenant démarrer Tails en [[mode hors-ligne|doc/first_steps/"
"startup_options/offline_mode]], ce qui désactive toute connexion réseau pour "
"plus de sécurité. Ceci peut être utile pour travailler sur des documents "
"sensibles."

#. type: Bullet: '- '
msgid ""
"We added <span class=\"application\">[[Icedove|doc/anonymous_internet/"
"icedove]]</span>, a rebranded version of the <span class=\"application"
"\">Mozilla Thunderbird</span> email client."
msgstr ""
"Nous avons ajouté le client mail <span class=\"application\">[[Icedove|doc/"
"anonymous_internet/icedove]]</span>, une version de <span class=\"application"
"\">Mozilla Thunderbird</span> avec un nom différent."

#. type: Plain text
#, no-wrap
msgid ""
"  <span class=\"application\">Icedove</span> is currently a technology preview. It is safe to use in the\n"
"  context of Tails but it will be better integrated in future versions\n"
"  until we remove <span class=\"application\">[[Claws\n"
"  Mail|doc/anonymous_internet/claws_mail]]</span>. Users of <span class=\"application\">Claws\n"
"  Mail</span> should refer to our instructions to [[migrate their data from\n"
"  <span class=\"application\">Claws Mail</span> to\n"
"  <span class=\"application\">Icedove</span>|doc/anonymous_internet/claws_mail_to_icedove]].\n"
msgstr ""
"  <span class=\"application\">Icedove</span> est actuellement en cours de mise en place\n"
"  dans Tails. Il est déjà possible de l'utiliser de manière sécurisée mais il sera mieux intégré\n"
"  dans les futures versions, jusqu'au retrait de <span class=\"application\">[[Claws\n"
"  Mail|claws_mail]]</span>. Les utilisateurs de <span class=\"application\">Claws\n"
"  Mail</span> devraient consulter nos instructions pour [[migrer leurs données\n"
"  de <span class=\"application\">Claws Mail</span> vers\n"
"  <span class=\"application\">Icedove</span>|doc/anonymous_internet/claws_mail_to_icedove]].\n"

#. type: Title ##
#, no-wrap
msgid "Upgrades and changes"
msgstr "Mises à jour et changements"

#. type: Bullet: '- '
msgid ""
"Improve the wording of the first screen of <span class=\"application\">Tails "
"Installer</span>."
msgstr ""
"Amélioration de la formulation du premier écran de l'<span class="
"\"application\">Installeur Tails</span>."

#. type: Bullet: '- '
msgid ""
"Restart Tor automatically if connecting to the Tor network takes too long. "
"([[!tails_ticket 9516 desc=\"#9516\"]])"
msgstr ""
"Redémarrage automatique de Tor si la connexion au réseau Tor est trop "
"longue. (Ticket résolu : [[!tails_ticket 9516 desc=\"#9516\"]])"

#. type: Bullet: '- '
msgid ""
"Update several firmware packages which might improve hardware compatibility."
msgstr ""
"Mise à jour de plusieurs pilotes matériels (firmware), ce qui améliore la "
"compatibilité matérielle."

#. type: Plain text
msgid "- Update the Tails signing key which is now valid until 2017."
msgstr ""
"- Mise à jour de la clé de signature de Tails, qui est maintenant valide "
"jusqu'en 2017."

#. type: Bullet: '- '
msgid "Update <span class=\"application\">Tor Browser</span> to 5.0.4."
msgstr ""
"Mise à jour du <span class=\"application\">Navigateur Tor</span> vers la "
"version 5.0.4."

#. type: Plain text
msgid "- Update Tor to 0.2.7.4."
msgstr "- Mise à jour de Tor vers la version 0.2.7.4."

#. type: Title ##
#, no-wrap
msgid "Fixed problems"
msgstr "Problèmes résolus"

#. type: Bullet: '- '
msgid ""
"Prevent <span class=\"command\">wget</span> from leaking the IP address when "
"using the FTP protocol. ([[!tails_ticket 10364 desc=\"#10364\"]])"
msgstr ""
"L'usage du protocole FTP avec <span class=\"command\">wget</span> ne révèle "
"plus l'adresse IP. (Ticket résolu : [[!tails_ticket 10364 desc=\"#10364\"]])"

#. type: Bullet: '- '
msgid ""
"Prevent symlink attack on <span class=\"filename\">~/.xsession-errors</span> "
"via <span class=\"command\">tails-debugging-info</span> which could be used "
"by the amnesia user to bypass read permissions on any file.  ([[!"
"tails_ticket 10333 desc=\"#10333\"]])"
msgstr ""
"Prévention de l'attaque par lien symbolique (*symlink attack*) sur le "
"fichier <span class=\"filename\">~/.xsession-errors</span> via la commande "
"<span class=\"command\">tails-debugging-info</span>, qui pouvait être "
"utilisée par l'utilisateur amnesia pour contourner les droits d'accès en "
"lecture à tout autre fichier (Ticket résolu : [[!tails_ticket 10333 desc="
"\"#10333\"]])"

#. type: Bullet: '- '
msgid ""
"Force synchronization of data on the USB stick at the end of automatic "
"upgrades. This might fix some reliability bugs in automatic upgrades."
msgstr ""
"Force la synchronisation des données sur la clé USB à la fin des mises à "
"jour automatique. Cela pourrait résoudre certains bugs apparaissant lors "
"d'une mise à jour automatique."

#. type: Plain text
msgid "- Make the \"I2P is ready\" notification more reliable."
msgstr "- La notification \"I2P est prêt\" est désormais plus fiable."
