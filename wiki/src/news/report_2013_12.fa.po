# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-07-28 09:53+0200\n"
"PO-Revision-Date: 2015-10-22 14:22+0000\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian "
"<http://weblate.451f.org:8889/projects/tails/report_2013_12/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails report for December, 2013\"]]\n"
msgstr "[[!meta title=\"گزارش دسامبر ۲۰۱۳ تیلز\"]]\n"

#. type: Title =
#, no-wrap
msgid "Releases\n"
msgstr "انتشارها\n"

#. type: Plain text
msgid "Tails 0.22 was released on December 11."
msgstr "تیلز ۰٫۲۲ روز ۱۱ دسامبر منتشر شد."

#. type: Plain text
msgid ""
"Tails 0.22.1 is scheduled for January 21. The schedule for the next releases "
"is on our [[contribute/calendar]]."
msgstr ""
"تیلز ۰٫۲۲٫۱ قرار است ۲۱ ژانویه منتشر شود. برنامهٔ انتشارهای بعدی در "
"[[contribute/calendar]] ما است."

#. type: Title =
#, no-wrap
msgid "Metrics\n"
msgstr "متریک‌ها\n"

#. type: Bullet: '- '
msgid ""
"Tails has been started more than 218 512 times in December.  This make 7 049 "
"boots a day in average."
msgstr ""
"تیلز بیش از ۲۱۸٫۵۱۲  بار در ماه دسامبر راه‌اندازی شد. یعنی به طور متوسط ۷٫"
"۰۴۹ راه‌اندازی در روز."

#. type: Bullet: '- '
msgid "17 791 downloads of the OpenPGP signature of Tails ISO."
msgstr "- ۱۷٫۷۹۱ دانلود امضای اُپن‌پی‌جی‌پی ایزوی تیلز."

#. type: Bullet: '- '
msgid "103 reports were received through WhisperBack."
msgstr "۱۰۳ گزارش از طریق ویسپربک دریافت شدند."

#. type: Title =
#, no-wrap
msgid "Code\n"
msgstr "کد\n"

#. type: Plain text
msgid "New features:"
msgstr "ویژگی‌های جدید:"

#. type: Bullet: '- '
msgid ""
"Huge progress was made on the [[!tails_gitweb_branch feature/spoof-mac "
"desc=\"MAC spoofing feature\"]], that [[can now be tested|news/spoof-mac]] "
"([[!tails_ticket 5421]])."
msgstr ""
"پیشرفت بیساری در [[!tails_gitweb_branch feature/spoof-mac desc=\"ویژگی جا "
"زدن مک\"]] ایجاد شد و [[حالا می‌توان آن را امتحان کرد|news/spoof-mac]] ([["
"!tails_ticket 5421]])."

#. type: Bullet: '- '
msgid ""
"Experimental UEFI support was completed and is been tested. A bit more work "
"[[!tails_ticket 5739 desc=\"is needed\"]], though."
msgstr ""
"پشتیبانی آزمایشی از UEFI کامل و آزمایش شد. اما هنوز کار بیشتری [["
"!tails_ticket 5739 desc=\"لازم دارد\"]]."

#. type: Bullet: '- '
msgid ""
"The last mile was basically [[!tails_gitweb_branch "
"feature/incremental-upgrades-integration desc=\"completed\"]] regarding "
"incremental upgrades, that will be enabled by default starting with Tails "
"0.22.1 ([[!tails_ticket 6014]])."
msgstr ""
"آخرین قدم برای ارتقاهای تدریجی [[!tails_gitweb_branch feature/incremental-"
"upgrades-integration desc=\"برداشته شد\"]] و این ویژگی از تیلز ۰٫۲۲٫۱ به طور "
"پیش‌فرض فعال خواهد بود ([[!tails_ticket 6014]])."

#. type: Plain text
msgid "Work in progress:"
msgstr "کارهای در حال انجام:"

#. type: Bullet: '- '
msgid ""
"David Wolinsky has [started "
"porting](https://mailman.boum.org/pipermail/tails-dev/2013-December/004517.html)  "
"the WiNoN design to Tails: multiple, independent VMs connected to "
"independent paths through the Tor network in order to wear multiple "
"hats. Also, as David put it: \"There are other benefits of using VMs as the "
"Whonix folks have recognized\" ([[!tails_ticket 5748]])."
msgstr ""
"دیوید والینسکی [شروع به پورت کردن](https://mailman.boum.org/pipermail/tails-"
"dev/2013-December/004517.html) طراحی WiNoN به تیلز کرده‌است: ماشین‌های مجازی "
"مستقل متصل به مسیرهای مستقل شبکه تور برای استفاده از کلاه‌های مختلف. همچنین "
"به قول دیوید «دوستان در Whonix مزایای دیگری نیز برای استفاده از ماشین‌های "
"مجازی یافته‌اند» ([[!tails_ticket 5748]])."

#. type: Bullet: '- '
msgid ""
"We have struggled against a [[!tails_ticket 6460 desc=\"memory wipe "
"regression\"]] on some hardware with recent Linux kernels. No success so "
"far."
msgstr ""
"در حال تلاش برای حل کردن یک [[!tails_ticket 6460 desc=\"memory wipe "
"regression\"]] روی بعضی سخت‌افزارها با هسته‌های جدید لینوکس هستیم. تا به حال "
"موفق به رفع این مشکل نشده‌ایم."

#. type: Bullet: '- '
msgid ""
"Some progress was made towards the [[!tails_gitweb_branch feature/wheezy "
"desc=\"migration to Wheezy\"]] (Tails 1.1)."
msgstr ""
"پیشرفت‌هایی در راه [[!tails_gitweb_branch feature/wheezy desc=\"کوچ به "
"Wheezy\"]] (تیلز ۱٫۱) به دست آمد."

#. type: Bullet: '- '
msgid "Early support for [Monkeysign](http://web.monkeysphere.info/monkeysign/) was"
msgstr ""
"پشتیبانی ابتدایی از [Monkeysign](http://web.monkeysphere.info/monkeysign/)"

#. type: Plain text
#, no-wrap
msgid "  [[!tails_gitweb_branch feature/monkeysign desc=\"merged\"]]\n"
msgstr "  [[!tails_gitweb_branch feature/monkeysign desc=\"ادغام شد\"]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"  ([[!tails_ticket 6455]]), but [[!tails_ticket 6515 desc=\"more work\n"
"  is needed\"]].\n"
msgstr ""
"  ([[!tails_ticket 6455]])، اما [[!tails_ticket 6515 desc=\"کار بیشتری\n"
"  نیاز دارد\"]].\n"

#. type: Plain text
msgid "Bug and regression fixes:"
msgstr "رفع ایرادها و رگرسیون‌ها:"

#. type: Bullet: '- '
msgid ""
"[[!tails_gitweb_branch bugfix/6468-disable-webrtc desc=\"Disable WebRTC\"]] "
"([[!tails_ticket 6468]])."
msgstr ""
"[[!tails_gitweb_branch bugfix/6468-disable-webrtc desc=\"غیرفعال کردنWebRTC\""
"]] ([[!tails_ticket 6468]])."

#. type: Bullet: '- '
msgid ""
"[[!tails_gitweb_branch bugfix/6478 desc=\"Fix keyboard shortcuts\"]] "
"([[!tails_ticket 6478)."
msgstr ""
"[[!tails_gitweb_branch bugfix/6478 desc=\"درست کردن میان‌برهای صفحه‌کلید\"]] "
"([[!tails_ticket 6478)."

#. type: Bullet: '- '
msgid ""
"[[!tails_gitweb_branch bugfix/6477-htpdate-user-agent desc=\"Use the same "
"User-Agent in htpdate as in the Tor Browser\"]] ([[!tails_ticket 6477]])."
msgstr ""
"[[!tails_gitweb_branch bugfix/6477-htpdate-user-agent desc=\"استفاده از همان "
"عامل کاربر مرورگر تور در htpdate\"]] ([[!tails_ticket 6477]])."

#. type: Bullet: '- '
msgid ""
"[[!tails_gitweb_branch bugfix/unsafe-browser-vs.-FF24 desc=\"Fix the Unsafe "
"Browser configuration\"]] ([[!tails_ticket 6479]])."
msgstr ""
"[[!tails_gitweb_branch bugfix/unsafe-browser-vs.-FF24 desc=\"رفع مشکل "
"پیکربندی «مرورگر غیرامن»\"]] ([[!tails_ticket 6479]])."

#. type: Bullet: '- '
msgid ""
"[[!tails_gitweb_branch bugfix/6536-IE-icon-in-Windows-camouflage-mode "
"desc=\"Set the browser icon to IE's one in Windows camouflage mode\"]]."
msgstr ""
"[[!tails_gitweb_branch bugfix/6536-IE-icon-in-Windows-camouflage-mode desc=\""
"تغییر نمایهٔ مرورگر به نمایهٔ IE در حالت استتار ویندوزی\"]]."

#. type: Plain text
msgid "Branches pending review:"
msgstr "بررسی شاخه‌های نیمه‌کاره:"

#. type: Bullet: '- '
msgid ""
"[[!tails_gitweb_branch feature/amd64-kernel desc=\"Install a 64-bit "
"kernel\"]]."
msgstr ""
"[[!tails_gitweb_branch feature/amd64-kernel desc=\"نصب یک هستهٔ ۶۴بیتی\"]]."

#. type: Bullet: '- '
msgid ""
"[[!tails_gitweb_branch feature/poedit-from-backports desc=\"Install poedit "
"from official backports\"]] was proposed ([[!tails_ticket 6456]])."
msgstr ""
"پیشنهاد [[!tails_gitweb_branch feature/poedit-from-backports desc=\"نصب "
"poedit از بک‌پورت‌های رسمی\"]] مطرح شد ([[!tails_ticket 6456]])."

#. type: Bullet: '- '
msgid ""
"[[!tails_gitweb_branch 5588-no-autologin-consoles desc=\"Do not create "
"auto-login text consoles\"]] ([[!tails_ticket 5588]])."
msgstr ""
"[[!tails_gitweb_branch 5588-no-autologin-consoles desc=\"کنسول‌های متنی ورود "
"خودکار ایجاد نکنید\"]] ([[!tails_ticket 5588]])."

#. type: Plain text
msgid "And also:"
msgstr "همچنین:"

#. type: Bullet: '- '
msgid ""
"[[!tails_gitweb_branch bugfix/tor-0.2.4-is-stable desc=\"Tor 0.2.4 is now "
"stable!\"]]"
msgstr ""
"[[!tails_gitweb_branch bugfix/tor-0.2.4-is-stable desc=\"تور ۰٫۲٫۴ حالا "
"پایدار است!\"]]"

#. type: Bullet: '- '
msgid ""
"The Persistent Volume Assistant now [[!tails_ticket 5311 desc=\"displays "
"nicer paths\"]]. Thanks to Andres Gomez!"
msgstr ""
"راهنمای درایو مانا حالا [[!tails_ticket 5311 desc=\"مسیرهای بهتری نشان می‌"
"دهد\"]]. با تشکر از آندرس گومز!"

#. type: Bullet: '- '
msgid ""
"Torbutton was [[!tails_gitweb_branch feature/torbutton-1.6.5.3 "
"desc=\"upgraded\"]] to 1.6.5.3 ([[!tails_ticket 6566]])."
msgstr ""
"تورباتن به ۱٫۶٫۵٫۳ [[!tails_gitweb_branch feature/torbutton-1.6.5.3 desc=\""
"upgraded\"]] ارتقاء داده شد ([[!tails_ticket 6566]])."

#. type: Bullet: '- '
msgid ""
"Our Tor Browser build and runtime dependencies [[!tails_gitweb_branch "
"feature/torbrowser-24.2.0esr-1+tails1 desc=\"were updated\"]]."
msgstr ""
"ساخت مرورگر تور ما و وابستگی‌های زمان اجرای آن [[!tails_gitweb_branch "
"feature/torbrowser-24.2.0esr-1+tails1 desc=\"ارتقاء یافتند\"]]."

#. type: Bullet: '- '
msgid ""
"We have fixed various NSS security issues in squeeze-backports "
"([[!tails_ticket 6497]])."
msgstr ""
"چندین مشکل امنیتی ان‌اس‌اس را در squeeze-backportsحل کردیم ([[!tails_ticket "
"6497]])."

#. type: Title =
#, no-wrap
msgid "Documentation and website\n"
msgstr "مستندسازی و تارنما\n"

#. type: Bullet: '- '
msgid ""
"A branch to [[!tails_gitweb_branch feature/cleanup-ikiwiki-setup \"clean up "
"our ikiwiki configuration\"]] was started."
msgstr ""
"شاخه‌ای برای [[!tails_gitweb_branch feature/cleanup-ikiwiki-setup \"مرتب "
"کردن پیکربندی ikiwikiمان\"]] راه‌اندازی شد."

#. type: Bullet: '- '
msgid ""
"The [[Mac installation "
"instructions|doc/first_steps/installation/manual/mac]] were made a bit "
"safer."
msgstr ""
"[[راهنمای نصب مک|doc/first_steps/installation/manual/mac]] کمی امن‌تر شد."

#. type: Bullet: '- '
msgid "The links to files and branches in cgit were fixed."
msgstr "پیوندها به فایل‌ها و شاخه‌ها در cgit درست شدند."

#. type: Bullet: '- '
msgid ""
"The tails-support mailing-list is now mentioned on [[Help other Tails "
"users|contribute/how/help]]."
msgstr ""
"حالا در فهرست رایانامهٔ tails-support به [[کمک به کاربران دیگر "
"تیلز|contribute/how/help]] اشاره می‌شود."

#. type: Bullet: '- '
msgid "The documentation for incremental upgrades was written."
msgstr "مستندات ارتقاهای تدریجی نوشته شدند."

#. type: Bullet: '- '
msgid "The documentation for MAC spoofing was drafted."
msgstr "مستندات جازدن آدرس مک پیش‌نویس شد."

#. type: Bullet: '- '
msgid "The [[draft FAQ|blueprint/faq]] has now more content."
msgstr "[[پیش‌نویس پرسش و پاسخ‌های متداول|blueprint/faq]] حالا دارای محتوا است."

#. type: Title =
#, no-wrap
msgid "Infrastructure\n"
msgstr "زیرساخت\n"

#. type: Title -
#, no-wrap
msgid "Test suite\n"
msgstr "بسته آزمایشی\n"

#. type: Bullet: '- '
msgid ""
"The Tails automated test suite [[!tails_gitweb_branch test/rjb-migration "
"desc=\"can now be run\"]] on pure Debian Wheezy with backports "
"([[!tails_ticket 6399]]). This allowed us to update the test suite to match "
"current code, fix many bugs in it, and improve style a bit. Most of this was "
"merged, but a few more branches are pending review: [[!tails_ticket 5959]], "
"[[!tails_ticket 5465]], and [[!tails_ticket 6544]]."
msgstr ""
"بستهٔ آزمایشی خودکار تیلز را حالا می‌توان با بک‌پورت‌ها روی Debian Wheezy "
"خالص [[!tails_gitweb_branch test/rjb-migration desc=\"اجرا کرد\"]] ([["
"!tails_ticket 6399]]). این کار به ما اجازهٔ ارتقای بستهٔ آزمایشی برای تطابق "
"با کد فعلی، برطرف کردن بسیاری از ایرادها در آن و بهبود سبک آن را می‌دهد. "
"بیشتر آن ادغام شده، اما هنوز چند شاخه منتظر بررسی هستند: [[!tails_ticket "
"5959]]، [[!tails_ticket 5465]] و [[!tails_ticket 6544]]."

#. type: Bullet: '- '
msgid ""
"Our automated test suite was [[!tails_gitweb_branch feature/wheezy "
"desc=\"partially ported to the feature/wheezy branch\"]]."
msgstr ""
"بخشی از بستهٔ آزمایشی خودکار ما [[!tails_gitweb_branch feature/wheezy desc=\""
"به شاخهٔ feature/wheezy پورت شد\"]]."

#. type: Title -
#, no-wrap
msgid "Build system\n"
msgstr "سیستم ساخت\n"

#. type: Bullet: '- '
msgid ""
"Thanks to David Wolinsky and others, our Vagrant setup "
"[[!tails_gitweb_branch bugfix/6221-support-newer-vagrant desc=\"was "
"updated\"]] to work with newer Vagrant ([[!tails_ticket 6221]]), and the "
"corresponding basebox updated to include up-to-date Debian archive "
"keys. While we were at it, a few lurking bugs were fixed."
msgstr ""
"به لطف دیوید والینسکی و دیگران، تنظیمات واگرانت ما [[!tails_gitweb_branch "
"bugfix/6221-support-newer-vagrant desc=\"ارتقاء داده شدند\"]] تا به واگرانت "
"جدیدتر کار کنند ([[!tails_ticket 6221]])، و بیس‌باکس‌های مرتبط نیز برای "
"گنجاندن کلیدهای بایگانی به‌روز دبیان ارتقاء یافتند. در حین انجام این کارها "
"چند ایراد آزاردهنده را نیز رفع کردیم."

#. type: Bullet: '- '
msgid ""
"Thanks to WinterFairy, [it is now "
"easy](https://git-tails.immerda.ch/winterfairy/tails/log/?h=feature/import-translations-extern)  "
"to import translations from Transifex into our various Git repositories."
msgstr ""
"به لطف WinterFairy حالا وارد کردن ترجمه‌ها از Transifex به مخازن مختلف گیت "
"خودمان [کاری آسان است](https://git-"
"tails.immerda.ch/winterfairy/tails/log/?h=feature/import-translations-"
"extern)."

#. type: Title =
#, no-wrap
msgid "On-going discussions\n"
msgstr "بحث‌های دنباله‌دار\n"

#. type: Bullet: '- '
msgid ""
"[Tor Browser branding in "
"Tails?](https://mailman.boum.org/pipermail/tails-dev/2013-December/004362.html)"
msgstr ""
"[مرورگر تور در تیلز؟](https://mailman.boum.org/pipermail/tails-"
"dev/2013-December/004362.html)"

#. type: Bullet: '- '
msgid ""
"[Risks of enabled/disabled TCP "
"timestamps?](https://mailman.boum.org/pipermail/tails-dev/2013-December/004520.html)  "
"([[!tails_ticket 6579]])"
msgstr ""
"[خطرات فعال/غیرفعال کردن مُهر زمانی قرارداد هدایت "
"انتقال؟](https://mailman.boum.org/pipermail/tails-"
"dev/2013-December/004520.html)  ([[!tails_ticket 6579]])"

#. type: Title =
#, no-wrap
msgid "Funding\n"
msgstr "جذب سرمایه\n"

#. type: Bullet: '- '
msgid ""
"The Freedom of the Press Foundation launched a [campaign to support "
"encryption tools for "
"journalists](https://pressfreedomfoundation.org/). Tails is among the "
"projects this campaign gathers fund for."
msgstr ""
"بنیاد آزادی نشر [campaign to support encryption tools for "
"journalists](https://pressfreedomfoundation.org/) راه‌اندازی کرده است. تیلز "
"در بین پروژه‌هایی قرار دارد که این کارزار در تلاش برای جذب کمک‌‌های مالی "
"برایشان است."

#. type: Bullet: '- '
msgid "The proposal we have sent to sponsor Echo was accepted."
msgstr "پیشنهادی که به اسپانسر اکو فرستاده بودیم پذیرفته شد."

#. type: Bullet: '- '
msgid "Our grant proposal with sponsor Charlie was rejected."
msgstr "پیشنهاد اصلی ما برای اسپانسر چارلی رد شد."

#. type: Bullet: '- '
msgid "We are slowly making progress on our grant proposal with sponsor Golf."
msgstr "به آرامی در حال پیشبرد پیشنهاد اصلی خود برای اسپانسر گلف هستیم."

#. type: Bullet: '- '
msgid "We have almost completed a proposal to be sent to sponsor Lima."
msgstr "کار یک پیشنهاد برای اسپانسر لیما را تقریباً تمام کرده‌ایم."

#. type: Bullet: '- '
msgid "Our contract with sponsor Bravo is now finished."
msgstr "کار قرارداد ما با اسپانسر براوو به پایان رسید."

#. type: Bullet: '- '
msgid "Tails will soon accept donations in currencies other than Bitcoin."
msgstr ""
"تیلز به زودی کمک‌های مالی به روش‌های دیگری به جز بیت‌کوین نیز دریافت خواهد "
"کرد."

#. type: Bullet: '- '
msgid ""
"We are now very likely to create a non-profit organization dedicated to "
"Tails."
msgstr ""
"حالا احتمال زیادی وجود دارد که یک سازمان غیرانتفاعی مختص تیلز بنیان بگذاریم."

#. type: Bullet: '- '
msgid ""
"We have almost wrapped-up our bounties program. A report will be published "
"soonish."
msgstr ""
"برنامه‌های جایزه‌بگیری خود را تقریباً‌ تمام کرده‌ایم. به همین زودی‌ها گزارشی "
"در این مورد منتشر خواهد شد."

#. type: Title =
#, no-wrap
msgid "Outreach\n"
msgstr "توسعه\n"

#. type: Plain text
msgid ""
"Tails participated in the [30th Chaos Communication "
"Congress](https://events.ccc.de/congress/2013/wiki/Main_Page). It was a "
"great opportunity to meet, in person, a few existing and new contributors, "
"as well as many people we are working with."
msgstr ""
"تیلز در [سی‌امین Chaos Communication "
"Congress](https://events.ccc.de/congress/2013/wiki/Main_Page) شرکت کرد. این "
"کنگره فرصتی عالی برای ملاقات حضوری با چند نفر از مشارکت‌کنندگان قدیمی و جدید "
"و نیز بسیاری از افرادی که با آن‌ها کار می‌کنیم بود."

#. type: Plain text
msgid ""
"A self-organized event called *Tails needs your help* was organized "
"([slides](https://tails.boum.org/promote/slides/2013-12-29_-_Tails_needs_your_help.shtml)).  "
"It was a success considering the late notice."
msgstr ""
"رخدادی خودگردان با عنوان *تیلز به کمک شما نیاز دارد* سازمان‌دهی شد([اسلایدها]"
"(https://tails.boum.org/promote/slides/2013-12-29_-_Tails_needs_your_help.sht"
"ml)). با توجه به اعلان دیرهنگام آن می‌شود گفت رخداد موفقی بود."

#. type: Plain text
msgid "See you next year, probably with more space and events dedicated to Tails!"
msgstr ""
"سال دیگر شما را می‌بینیم؛ شاید در جایی بزرگ‌تر و رخدادهای بیشتر مرتبط با "
"تیلز!"

#. type: Title =
#, no-wrap
msgid "Press and testimonials\n"
msgstr "اخبار و قدردانی‌ها\n"

#. type: Plain text
#, fuzzy, no-wrap
msgid ""
"* 2013-12: Bruce Schneier\n"
"  "
"[answered](http://www.reddit.com/r/IAmA/comments/1r8ibh/iama_security_technologist_and_author_bruce/cdknf7a)\n"
"  to someone asking him what Linux distribution is its favorite: \"I don't\n"
"  use Linux. (Shhh. Don't tell anyone.) Although I have started using "
"Tails\".\n"
"* 2013-12-12: In [A conversation with Bruce\n"
"  "
"Schneier](http://boingboing.net/2013/12/15/bruce-schneier-and-eben-moglen-2.html),\n"
"  as part of the \"Snowden, the NSA and free software\" cycle at\n"
"  Columbia Law School NYC, Bruce Schneier says:\n"
"  - \"I think most of the public domain privacy tools are going to be\n"
"    safe, yes. I think GPG is going to be safe. I think OTR is going\n"
"    to be safe. I think that Tails is going to be safe. I do think\n"
"    that these systems, because they were not -- you know, the NSA has\n"
"    a big lever when a tool is written closed-source by a for-profit\n"
"    corporation. There are levers they have that they don't have in\n"
"    the open source international, altruistic community. And these are\n"
"    generally written by crypto-paranoids, they're pretty well\n"
"    designed. We make mistakes, but we find them and we correct them,\n"
"    and we're getting good at that. I think that if the NSA is going\n"
"    after these tools, they're going after implementations.\"\n"
"  - \"What do I trust? I trust, I trust Tails, I trust GPG [...]\"\n"
"  - \"We can make it harder, we can make it more expensive, we can make\n"
"    it more risky. And yes, every time we do something to increase one\n"
"    of those, we're making ourselves safer. [...] There are tools we\n"
"    are deploying in countries all over the world, that are keeping\n"
"    people alive. Tor is one of them. I mean, Tor saves lives. [...]\n"
"    And every time you use Tor [...] provides cover for everyone else\n"
"    who uses Tor [...]\"\n"
"* Jacob Appelbaum stated at the [Chaos Communication\n"
"  "
"Congress](https://events.ccc.de/congress/2013/Fahrplan/events/5713.html):\n"
"  \"if you are a journalist and you are not using Tails, you should\n"
"  probably be using Tails, unless you *really* know what\n"
"  you're doing\".\n"
msgstr ""
"* ۲۰۱۳/۱۲: بروش اشنیر \n"
"  [answered](http://www.reddit.com/r/IAmA/comments/1r8ibh/iama_security_techn"
"ologist_and_author_bruce/cdknf7a)\n"
"  to someone asking him what Linux distribution is its favorite: \"I don't\n"
"  use Linux. (Shhh. Don't tell anyone.) Although I have started using Tails\""
".\n"
"* 2013-12-12: In [A conversation with Bruce\n"
"  Schneier](http://boingboing.net/2013/12/15/bruce-schneier-and-eben-"
"moglen-2.html),\n"
"  as part of the \"Snowden, the NSA and free software\" cycle at\n"
"  Columbia Law School NYC, Bruce Schneier says:\n"
"  - \"I think most of the public domain privacy tools are going to be\n"
"    safe, yes. I think GPG is going to be safe. I think OTR is going\n"
"    to be safe. I think that Tails is going to be safe. I do think\n"
"    that these systems, because they were not -- you know, the NSA has\n"
"    a big lever when a tool is written closed-source by a for-profit\n"
"    corporation. There are levers they have that they don't have in\n"
"    the open source international, altruistic community. And these are\n"
"    generally written by crypto-paranoids, they're pretty well\n"
"    designed. We make mistakes, but we find them and we correct them,\n"
"    and we're getting good at that. I think that if the NSA is going\n"
"    after these tools, they're going after implementations.\"\n"
"  - \"What do I trust? I trust, I trust Tails, I trust GPG [...]\"\n"
"  - \"We can make it harder, we can make it more expensive, we can make\n"
"    it more risky. And yes, every time we do something to increase one\n"
"    of those, we're making ourselves safer. [...] There are tools we\n"
"    are deploying in countries all over the world, that are keeping\n"
"    people alive. Tor is one of them. I mean, Tor saves lives. [...]\n"
"    And every time you use Tor [...] provides cover for everyone else\n"
"    who uses Tor [...]\"\n"
"* Jacob Appelbaum stated at the [Chaos Communication\n"
"  Congress](https://events.ccc.de/congress/2013/Fahrplan/events/5713.html):\n"
"  \"if you are a journalist and you are not using Tails, you should\n"
"  probably be using Tails, unless you *really* know what\n"
"  you're doing\".\n"
