# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-12-24 15:03+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"state-image debian expert dvd\">[[!img install/inc/infography/tails-usb.png link=\"no\"]]</div>\n"
"<div class=\"state-image windows linux mac-usb\">[[!img install/inc/infography/final-tails.png link=\"no\"]]</div>\n"
"<div class=\"state-image install-clone mac-clone\">[[!img install/inc/infography/new-tails.png link=\"no\"]]</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"trophy\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<p>Yeah, you are done installing Tails!</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>If you want to save some of your documents\n"
"and configuration in an encrypted storage on the <span class=\"usb\">final</span><span class=\"clone\">new</span> Tails USB stick, follow\n"
"our instructions until the end. Otherwise, have a look at our\n"
"<span class=\"install-clone\">[[final recommendations|clone#recommendations]].</span>\n"
"<span class=\"expert\">[[final recommendations|expert/usb#recommendations]].</span>\n"
"<span class=\"debian\">[[final recommendations|debian/usb#recommendations]].</span>\n"
"<span class=\"windows\">[[final recommendations|win/usb#recommendations]].</span>\n"
"<span class=\"mac-usb\">[[final recommendations|mac/usb#recommendations]].</span>\n"
"<span class=\"mac-clone\">[[final recommendations|mac/clone#recommendations]].</span>\n"
"<span class=\"mac-dvd\">[[final recommendations|mac/dvd#recommendations]].</span>\n"
"<span class=\"linux\">[[final recommendations|linux/usb#recommendations]].</span>\n"
"</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr ""
