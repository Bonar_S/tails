[[!meta robots="noindex"]]
[[!meta title="Installing Tails on a USB stick from Linux"]]
[[!meta stylesheet="bootstrap" rel="stylesheet" title=""]]
[[!meta stylesheet="inc/stylesheets/assistant" rel="stylesheet" title=""]]
[[!meta stylesheet="inc/stylesheets/dave" rel="stylesheet" title=""]]
[[!meta stylesheet="inc/stylesheets/steps" rel="stylesheet" title=""]]
[[!meta stylesheet="inc/stylesheets/linux" rel="stylesheet" title=""]]
[[!meta script="install/inc/js/dave"]]

[[!inline pages="install/inc/tails-installation-assistant.inline" raw="yes"]]

<div class="step-image">[[!img install/inc/infography/os-linux.png link="no"]]</div>

<p class="start">Start in Linux.</p>

[[!inline pages="install/inc/steps/download.inline" raw="yes"]]

[[!inline pages="install/inc/steps/install_intermediary_intro.inline" raw="yes"]]

1. Install <span class="application">GNOME Disks</span>.

   - If you are using the GNOME desktop environment, <span class="application">GNOME Disks</span> should be
     installed by default.

   - Otherwise, install the <span class="code">gnome-disk-utility</span> package using the
     usual installation method for your Linux distribution.

1. Start <span class="application">GNOME Disks</span> (also called <span class="application">Disks</span>).

   For example, you can choose <span class="menuchoice">
     <span class="guimenu">Applications</span>&nbsp;▸
     <span class="guisubmenu">Utilities</span>&nbsp;▸
     <span class="guimenuitem">Disks</span></span>.

   [[!img install/inc/icons/gnome-disks.png link="no"]]

   <div class="step-image">[[!img install/inc/infography/plug-first-usb.png link="no"]]</div>

1. Plug the first USB stick in the computer.

   A new drive appears in the left pane. Click on it.

   [[!img install/inc/screenshots/gnome_disks_drive.png class="screenshot" link="no"]]

   <div class="step-image">[[!img install/inc/infography/install-intermediary-tails.png link="no"]]</div>

1. Click on the [[!img lib/emblem-system.png alt="System" class="symbolic" link="no"]]
   button in the top-right corner and choose <span class="guimenuitem">Restore Disk Image&hellip;</span>.

   [[!img install/inc/screenshots/gnome_disks_system.png class="screenshot" link="no"]]

1. In the <span class="guilabel">Restore Disk Image</span> dialog, click on the file selector button and
   choose the ISO image that you downloaded earlier.

1. Click on the <span class="button">Start Restoring&hellip;</span> button.

1. In the confirmation dialog click <span class="button">Restore</span>.
   The installation takes a few minutes.

1. After the installation is finished, click on the
   [[!img lib/media-eject.png alt="Eject" class="symbolic" link="no"]] button.

[[!inline pages="install/inc/steps/install_intermediary_outro.inline" raw="yes"]]

[[!inline pages="install/inc/steps/switch.inline" raw="yes"]]

[[!inline pages="install/inc/steps/restart_first_time.inline" raw="yes"]]

[[!inline pages="install/inc/steps/install_final.inline" raw="yes"]]

[[!inline pages="install/inc/steps/restart_second_time.inline" raw="yes"]]

[[!inline pages="install/inc/steps/you_are_done.inline" raw="yes"]]

[[!inline pages="install/inc/steps/create_persistence.inline" raw="yes"]]
