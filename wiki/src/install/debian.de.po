# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-12-22 22:46+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Content of: outside any tag (error?)
msgid ""
"[[!meta robots=\"noindex\"]] [[!meta title=\"Ubuntu\"]] [[!meta stylesheet="
"\"bootstrap\" rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"inc/"
"stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet="
"\"inc/stylesheets/router-debian\" rel=\"stylesheet\" title=\"\"]] [[!inline "
"pages=\"install/inc/tails-installation-assistant.inline\" raw=\"yes\"]] "
"[[<span class=\"back\">Back</span>|install/os]]"
msgstr ""

#. type: Content of: <h1>
msgid "<strong>Ubuntu</strong>"
msgstr ""

#. type: Content of: <div><div>
msgid "[[!inline pages=\"install/inc/router/clone\" raw=\"yes\"]] [["
msgstr ""

#. type: Content of: <div><div><div><div><h3>
msgid "Install from another Tails"
msgstr ""

#. type: Content of: <div><div>
msgid "|install/debian/clone/overview]]"
msgstr ""

#. type: Content of: <div><div><h3>
msgid "Download and install"
msgstr ""

#. type: Content of: <div><div><h4>
msgid "You need:"
msgstr ""

#. type: Content of: <div><div><ul><li>
msgid "1 USB stick <small>(at least 4 GB)</small>"
msgstr ""

#. type: Content of: <div><div><ul><li>
msgid ""
"30&ndash;60 minutes to download Tails <small>[[!inline pages=\"inc/"
"stable_i386_iso_size\" raw=\"yes\"]]</small>"
msgstr ""

#. type: Content of: <div><div><ul><li>
msgid "25 minutes to install"
msgstr ""

#. type: Content of: <div><div>
msgid "[["
msgstr ""

#. type: Content of: <div><div><div><div><h3>
msgid "Install from Ubuntu"
msgstr ""

#. type: Content of: <div><div>
msgid "|debian/usb/overview]] [["
msgstr ""

#. type: Content of: <div><div><div><div><h3>
msgid "<small>Install from Ubuntu using the</small>"
msgstr ""

#. type: Content of: <div><div><div><div><h3>
msgid "command line <small> and </small> GnuPG"
msgstr ""

#. type: Content of: <div><div>
msgid "|expert/usb/overview]]"
msgstr ""

#. type: Content of: <div><div><small>
msgid "<small> You can also:"
msgstr ""

#. type: Content of: <div><div><small><ul><li>
msgid "[[Burn Tails on a DVD|dvd]]"
msgstr ""

#. type: Content of: <div><div><small><ul><li>
msgid "[[Run Tails in a virtual machine|vm]]"
msgstr ""

#. type: Content of: <div><div>
msgid "</small>"
msgstr ""
